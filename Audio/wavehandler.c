#include "wavehandler.h"
#include "string.h"

// Private methods
uint8_t WaveHandler_parseHeader(WaveHandler* me);
uint8_t WaveHandler_readSector(WaveHandler* me);

/*
 * User Manual:
 *
 * Before using any of these functions FatFs_LinkDriver must be called in the main
 * and then f_mount must be called in the thread
 *
 * 1. Call setFileName
 * 2. Call initialise
 * 3. Call getNext regularly
 */

// Setup the Wave Handler with a file name and the Audio selector
void WaveHandler_create(WaveHandler* me, const char* name, uint8_t* s)
{
	strcpy(me->fileName, name);
	me->selector = s;
}

// Parse header and start file reading
uint8_t WaveHandler_initialise(WaveHandler* me)
{
	me->index = 0;
	me->currentSector = pvPortMalloc(SECTOR_SIZE);
	me->bytesRead = 0;

	// If the header of the file can be parsed
	if(WaveHandler_parseHeader(me) == WAV_OK)
	{
		// The current sector must have allocated memory
		if(me->currentSector != 0)
		{
			// And the reading of the file must be succesful
			if(WaveHandler_readSector(me) == WAV_OK)
			{
				return WAV_OK;
			}
		}
	}

	return WAV_NOT_OK;
}

// Returns the indexed value in the sector
audio_data WaveHandler_getNext(WaveHandler* me)
{
	// Number of tries when a file fails to be read
	uint32_t i = 0;

	// Data to be returned
	audio_data ret = me->currentSector[me->index];
	ret |= me->currentSector[me->index+1] << 8;			// TODO do this for cases where audio_data is a different type
	me->index += 2;

	// If the index overflows
	if(me->index >= SECTOR_SIZE)
	{
		// Set to start of current sector
		me->index = 0;

		// And then reload the current sector, if it fails retry a number of times
		while(WaveHandler_readSector(me) == WAV_NOT_OK && i < NUMBER_OF_TRIES)
		{
			i++;
			me->bytesRead = 0;

			// If it still fails
			if(i == NUMBER_OF_TRIES)
			{
				// Set the audio selector's selection to internal
				*(me->selector) = 0;
				return 0;
			}

			// Restart reading and set back to set place in the file
			WaveHandler_parseHeader(me);
		}
	}
	return ret;
}

uint8_t WaveHandler_parseHeader(WaveHandler* me)
{
	uint16_t audioFormat;
	uint16_t numChannels;
	uint32_t sampleRate;
	uint16_t bitsPerSample;

	uint8_t ret = WAV_NOT_OK;
	FRESULT res;

	// If the file opens successfully
	res = f_open(&me->file, (TCHAR const*) me->fileName, FA_READ);
	if(res == FR_OK)
	{
		// Read the first sector from the file
		res = f_read(&me->file, me->currentSector, 44, (UINT *) &(me->bytesRead));
		if(res == FR_OK)
		{
			// Get values
			me->fileSize = *(uint16_t *) (me->currentSector + 4);
			me->fileSize |= (*(uint16_t *) (me->currentSector + 6)) << 16;
			me->fileSize += 8;	// Extra 4 bytes for chunk ID and chunk size

			audioFormat = *(uint16_t *) (me->currentSector + 20);

			numChannels = *(uint16_t *) (me->currentSector + 22);

			sampleRate = *(uint16_t *) (me->currentSector + 24);
			sampleRate |= (*(uint16_t *) (me->currentSector + 26)) << 16;

			bitsPerSample = *(uint16_t *) (me->currentSector + 34);

			// Check that our system supports this kind of wave file
			if(audioFormat==1 && numChannels==2 && sampleRate==22050 && bitsPerSample==16)
			{
				// TODO if it's not supported then down-sample / interpolate
				ret = WAV_OK;
			}
		}
	}
	return ret;
}

// Reads the next sector from the SD card
uint8_t WaveHandler_readSector(WaveHandler* me)
{
	FRESULT res;
	uint32_t bytesToRead;
	uint32_t bytesRead;
	uint8_t headerToDispose[44];
	uint8_t ret = WAV_NOT_OK;

	// Read how many bytes until end of file
	bytesToRead = me->fileSize - me->bytesRead;

	// System to loop the sample

	// Read beginning of file if at end
	if(me->fileSize == me->bytesRead)
	{
		// Close the file to reopen it
		f_close(&me->file);
		res = f_open(&me->file, (TCHAR const*) me->fileName, FA_READ);
		if(res == FR_OK)
		{
			// Read header to skip these 44 bytes
			res = f_read(&me->file, headerToDispose, 44, (UINT *) &(me->bytesRead));
			if(res == FR_OK)
			{
				// Read the beginning of the file
				res = f_read(&me->file, me->currentSector, SECTOR_SIZE, (UINT *) &(bytesRead));
				if(res == FR_OK)
				{
					// Update total amount of bytes read
					me->bytesRead += bytesRead;
					ret = WAV_OK;
				}
			}
		}
	}

	// If near the end of the file
	else if(bytesToRead < SECTOR_SIZE)
	{
		// Read the end of the file
		res = f_read(&me->file, me->currentSector, bytesToRead, (UINT *) &(bytesRead));
		if(res == FR_OK)
		{
			// Close the file to reopen it
			f_close(&me->file);
			res = f_open(&me->file, (TCHAR const*) me->fileName, FA_READ);

			if(res == FR_OK)
			{
				// Read header to skip these 44 bytes
				res = f_read(&me->file, headerToDispose, 44, (UINT *) &(me->bytesRead));
				if(res == FR_OK)
				{
					// Read the beginning of the file
					res = f_read(&me->file, &me->currentSector[bytesRead], SECTOR_SIZE-bytesToRead, (UINT *) &(bytesRead));
					if(res == FR_OK)
					{
						// Update total amount of bytes read
						me->bytesRead += bytesRead;
						ret = WAV_OK;
					}
				}
			}
		}
	}
	// Read an uninterrupted sector
	else
	{
		res = f_read(&me->file, me->currentSector, SECTOR_SIZE, (UINT *) &(bytesRead));
		if(res == FR_OK)
		{
			// Update total amount of bytes read
			me->bytesRead += bytesRead;
			ret = WAV_OK;
		}
	}

	// If something went wrong with the file close it
	if(ret == WAV_NOT_OK)
	{
		f_close(&me->file);
	}
	return ret;
}
