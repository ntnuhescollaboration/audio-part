#ifndef WAVEHANDLER_H
#define WAVEHANDLER_H

#include "audio_types.h"
#include "sd_diskio.h"
#include "audiohandler.h"

#define WAV_NOT_OK 1
#define WAV_OK 0

#define SECTOR_SIZE 4096
#define NUMBER_OF_TRIES 6

/*
 * WaveHandler Class
 *	fileName  : char[] 			for the name of the file
 *	bytesRead : uint32_t 		how many bytes in the file
 *								has the FatFs module finished reading
 *
 *	currentSector : uint8_t* 	pointer to the sector loaded by FatFs
 *	index 		  : uint32_t 	position in the currentSector
 *
 *	file		  : FIL			FatFs file
 *
 *	selector	  : uint8_t*	points to the selection in AudioSelector
 *								and changes to internal if there is an SD
 *								card failure. There should be a pointer
 *								to the AudioSelector but bidirectional inclusion
 *								is impossible in C
 *
 */
typedef struct WaveHandler_s
{
	char fileName[15];
	uint32_t fileSize;
	uint32_t bytesRead;

	uint8_t* currentSector;
	uint32_t index;

	FIL file;

	uint8_t* selector;
} WaveHandler;

// Public methods
void WaveHandler_create(WaveHandler* me, const char* name, uint8_t* s);
uint8_t WaveHandler_initialise(WaveHandler* me);
audio_data WaveHandler_getNext(WaveHandler* me);

#endif // WAVEHANDLER_H
