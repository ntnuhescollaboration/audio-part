#include "audiohandler.h"

// Link internal signal from memory to the handler
void AudioHandler_create(AudioHandler* me, audio_length l, audio_data* d)
{
	me->data = d;
	me->length = l;
	me->index = 0;
}

// Return the next value with looping
audio_data AudioHandler_getNext(AudioHandler* me)
{
	audio_data ret = me->data[me->index];
	me->index = (me->index+1) % me->length;
	return ret;
}
