/*
 * audiohandle.h
 *
 *  Created on: 13 Jun 2018
 *      Author: adamt
 */

#ifndef AUDIOHANDLER_H_
#define AUDIOHANDLER_H_

#include "audio_types.h"

/*
 *	AudioSample Class
 *
 *	length	: how many values in data
 *	index 	: current write position in the data
 *	data 	: array of audio PCM values
 *
 */
typedef struct AudioHandler_s
{
	audio_length length;
	audio_length index;
    audio_data* data;
} AudioHandler;

// Public methods

// Method for the creation of an audio sample
void AudioHandler_create(AudioHandler* me, audio_length l, audio_data* d);

// Method that advances through the audio sample
// and restarts when it gets to the end
audio_data AudioHandler_getNext(AudioHandler* me);


#endif /* AUDIOHANDLER_H_ */
