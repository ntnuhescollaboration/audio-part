/*
 * audioselector.h
 *
 *  Created on: 30 Jul 2018
 *      Author: adamt
 */

#ifndef AUDIOSELECTOR_H_
#define AUDIOSELECTOR_H_

#include "audio_types.h"
#include "audiohandler.h"
#include "wavehandler.h"

// Defines for the selection
#define INTERNAL 0
#define EXTERNAL 1

/*
 * AudioSelector Class
 *	selection : uint8_t 		select the file source
 *	internal  : AudioHandler 	internal audio wave source
 *	external  : WaveHandler		wave file source from SD card
 */
typedef struct AudioSelector_s
{
	uint8_t selection;
	AudioHandler internal;
	WaveHandler external;
} AudioSelector;

// Public methods
void AudioSelector_initialise(AudioSelector* me, const char* name, audio_length l, audio_data* data);
audio_data AudioSelector_getNext(AudioSelector* me);

#endif /* AUDIOSELECTOR_H_ */
