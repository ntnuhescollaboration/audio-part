/**
  ******************************************************************************
  * @file    stm32f7xx_it.c
  * @author  Ac6
  * @version V1.0
  * @date    02-Feb-2015
  * @brief   Default Interrupt Service Routines.
  ******************************************************************************
*/

/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_hal.h"
#include "stm32f7xx.h"
#ifdef USE_RTOS_SYSTICK
#include <cmsis_os.h>
#endif
#include "stm32f7xx_it.h"
#include "main.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
extern SAI_HandleTypeDef 				haudio_out_sai;
extern SD_HandleTypeDef 				uSdHandle;
extern TIM_HandleTypeDef		 		htim6;
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/**
  * @brief  This function handles SysTick Handler, but only if no RTOS defines it.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
#ifdef USE_RTOS_SYSTICK
	osSystickHandler();
#else
	HAL_IncTick();
	HAL_SYSTICK_IRQHandler();
#endif
}

void TIM6_DAC_IRQHandler(void)
{
#ifdef USE_RTOS_SYSTICK
	HAL_IncTick();
	HAL_TIM_IRQHandler(&htim6);
#endif
}

/******************************************************************************/
/*                 STM32F7xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f7xx.s).                                               */
/******************************************************************************/


/**
  * @brief  This function handles DMA2 Stream 1 interrupt request.
  * @param  None
  * @retval None
  */
void AUDIO_OUT_SAIx_DMAx_IRQHandler(void)
{
	HAL_DMA_IRQHandler(haudio_out_sai.hdmatx);
}

/**
 * @brief Handles SDMMC1 DMA Rx transfer interrupt request.
 * @retval None
 */
void BSP_SDMMC_DMA_Rx_IRQHandler(void)
{
	HAL_DMA_IRQHandler(uSdHandle.hdmarx);
}

/**
 * @brief Handles SD1 card interrupt request.
 * @retval None
 */
void BSP_SDMMC_IRQHandler(void)
{
	HAL_SD_IRQHandler(&uSdHandle);
}

void NMI_Handler(void)
{
}


/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}



/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}



/*
                    (%*@&......&...(
                  @.....................@
                ...@........................
               &.............................@
              ................................@
             &..............................@...
            #.............................@....,@
            #...%,,,,,,,,,@..,....,....,,..,,,,(
            @...........,,%,,,,((@@((@@ #,,,,,/
            @,,,...,,,@@(((((((@(((((((
                *@@@(((/(((((((@%(((((@
                    /&**//*****&@@(((%
                    *((./#*./'*#(/@/(&
      *@,,....,,,..,%**./'(#(*,/,,**(*,.......,,,,&@
  .,.....@           **%.**...........             (*...@
    @,,...,&         ................&    #@@%*,,...,*.
          &(,............................,,@#
                      ..,....%.......
                     ,..*..%.%..&....
               ,,,,,,,#..../.........,,,,,.
              ,,,,,,,,,,/,,@,,,@,,*,,,,,,,,,%
             (/,,,,,,,,,,,,,,,,,,,,,,,,,,,'/@
                /////,,,,,,,,,,,,,,,,/////
                    ,%(@ @///%///@


  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
	/* Go to infinite loop when Hard Fault exception occurs */

	// if not good, big problem
	while (1)
	{
	}
}


