/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/
			
// Includes
#include "main.h"
#include "color.h"
#include "stevenslaw.h"

#include "../Audio/audiohandler.h"
#include "../Audio/audiosamples.h"
#include "../Audio/wavehandler.h"
#include "../Audio/audioselector.h"

// Play buffer definition for the audio output
#define PLAY_BUFF_SIZE       2048
uint16_t                     PlayBuff[PLAY_BUFF_SIZE];

// Mail boxes
osMailQId sampleDMAQueue;
osMailQId colorQueue;
osMailQId sdWaitQueue;

// Thread & timer definitions
osThreadId audioHandle;
osThreadId colourGeneratorHandle;
osThreadId randomHandle;
osThreadId sdCardHandle;
static void audioThread(void const * argument);
static void colourGenThread(void const * argument);
static void messyThread(void const * argument);

// Functions
static void SystemClock_Config(void);
static void Playback_Init(void);
static void CPU_CACHE_Enable(void);

// Audio sample and SD card necessities
char SD_Path[4];
FATFS fs;
AudioSelector redSel;
AudioSelector greenSel;
AudioSelector blueSel;
AudioSelector whiteSel;

// Main function
int main(void)
{
	/* Enable the CPU Cache */
	CPU_CACHE_Enable();

	/* STM32F7xx HAL library initialization:
       - Configure the Flash prefetch
       - Systick timer is configured by default as source of time base, but user
         can eventually implement his proper time base source (a general purpose
         timer for example or other time source), keeping in mind that Time base
         duration should be kept 1ms since PPP_TIMEOUT_VALUEs are defined and
         handled in milliseconds basis.
       - Set NVIC Group Priority to 4
       - Low Level Initialization
    */
	HAL_Init();

	/* Configure the system clock to have a frequency of 216 MHz */
	SystemClock_Config();

	/* Configure LED2 */
	BSP_LED_Init(LED2);
	/* Configure LED1 */
	BSP_LED_Init(LED1);

	/* Initialize playback */
	Playback_Init();

	if(FATFS_LinkDriver(&SD_Driver, SD_Path) != 0)
	{
		// Failure to link SD card driver
		Error_Handler(ERROR_SD_DRIVER);
	}

	// Define mail box for updating sound
	osMailQDef(SamplePositionQueue, (uint32_t) 1, uint16_t);
	sampleDMAQueue = osMailCreate(osMailQ(SamplePositionQueue), NULL);

	osMailQDef(ColorQueue, (uint32_t) 1, Color);
	colorQueue = osMailCreate(osMailQ(ColorQueue), NULL);

	osMailQDef(SDQueue, (uint32_t) 1, uint32_t);
	sdWaitQueue = osMailCreate(osMailQ(SDQueue), NULL);

	// Thread definitions
	osThreadDef(Audio, audioThread, osPriorityNormal, 0, configMINIMAL_STACK_SIZE);
	audioHandle = osThreadCreate(osThread(Audio), NULL);

	osThreadDef(CameraSim, colourGenThread, osPriorityNormal, 0, configMINIMAL_STACK_SIZE);
	colourGeneratorHandle = osThreadCreate(osThread(CameraSim), NULL);

	osThreadDef(MessySim, messyThread, osPriorityNormal, 0, configMINIMAL_STACK_SIZE);
	randomHandle = osThreadCreate(osThread(MessySim), NULL);

	osKernelStart();

	// The program should never get here as the kernel takes over
	for(;;);
}

void audioThread(void const * argument)
{
  uint16_t* position;
  Color c;
  Color* pC;

  osEvent event;

  audio_data r;
  audio_data g;
  audio_data b;
  audio_data w;
  int32_t sat;

  // Initialize the data buffer
    for(int i=0; i < PLAY_BUFF_SIZE; i++)
    {
      PlayBuff[i] = 0;
    }

    c.rgbwValues.r = 0;
    c.rgbwValues.g = 0;
    c.rgbwValues.b = 0;
    c.rgbwValues.w = 0;

    // If no link to SD Card Driver, the AudioSelector_initialiser will detect it
    f_mount(&fs, (TCHAR const*)"",1);

    AudioSelector_initialise(&redSel, "red.wav", red_length, redIntern);
    AudioSelector_initialise(&greenSel, "green.wav", green_length, greenIntern);
    AudioSelector_initialise(&blueSel, "blue.wav", blue_length, blueIntern);
    AudioSelector_initialise(&whiteSel, "white.wav", white_length, whiteIntern);

  // Start loopback
  for(;;)
  {
    // Wait until DMA has finished a full or half transfer
    event = osMailGet(sampleDMAQueue, osWaitForever);
    if(event.status == osEventMail)
    {
      // If a full transfer has occurred, fill second half of buffer
      // If a half transfer has occurred, fill first half of buffer
      position = event.value.p;
      BSP_LED_Toggle(LED2);

      // If a colour has been updated then it can be saved
      event = osMailGet(colorQueue, 0);
      if(event.status == osEventMail)
      {
        pC = event.value.p;
        c = *pC;
        osMailFree(colorQueue, pC);
      }

      // Update the first or the second part of the buffer
      for(int i = 0; i < PLAY_BUFF_SIZE/2; i++)
      {
        r = (AudioSelector_getNext(&redSel)*stevensLawConversionTable[c.rgbwValues.r])/256;
        g = (AudioSelector_getNext(&greenSel)*stevensLawConversionTable[c.rgbwValues.g])/256;
        b = (AudioSelector_getNext(&blueSel)*stevensLawConversionTable[c.rgbwValues.b])/256;
        w = (AudioSelector_getNext(&whiteSel)*stevensLawConversionTable[c.rgbwValues.w])/256;

        sat = r+g+b+w;

        if(sat > AUDIO_MAX)
        {
        	PlayBuff[*position + i] = AUDIO_MAX;
        }
        else if(sat < AUDIO_MIN)
        {
        	PlayBuff[*position + i] = AUDIO_MIN;
        }
        else
        {
        	PlayBuff[*position + i] = sat;
        }
      }

      osMailFree(sampleDMAQueue, position);
      BSP_LED_Toggle(LED2);
    }
  }
}

void colourGenThread(void const * argument)
{
	uint8_t conc_index = 0;
	Color* c;
	Color concats[9];
	concats[0].rgbwValues.r = 250;
	concats[0].rgbwValues.b = 0;
	concats[0].rgbwValues.g = 0;
	concats[0].rgbwValues.w = 0;

	concats[1].rgbwValues.r = 0;
	concats[1].rgbwValues.g = 250;
	concats[1].rgbwValues.b = 0;
	concats[1].rgbwValues.w = 0;

	concats[2].rgbwValues.r = 0;
	concats[2].rgbwValues.g = 0;
	concats[2].rgbwValues.b = 250;
	concats[2].rgbwValues.w = 0;

	concats[3].rgbwValues.r = 0;
	concats[3].rgbwValues.g = 0;
	concats[3].rgbwValues.b = 0;
	concats[3].rgbwValues.w = 250;

	concats[4].rgbwValues.r = 100;
	concats[4].rgbwValues.g = 100;
	concats[4].rgbwValues.b = 0;
	concats[4].rgbwValues.w = 100;

	concats[5].rgbwValues.r = 100;
	concats[5].rgbwValues.g = 0;
	concats[5].rgbwValues.b = 100;
	concats[5].rgbwValues.w = 100;

	concats[6].rgbwValues.r = 100;
	concats[6].rgbwValues.g = 0;
	concats[6].rgbwValues.b = 0;
	concats[6].rgbwValues.w = 100;

	concats[7].rgbwValues.r = 0;
	concats[7].rgbwValues.g = 100;
	concats[7].rgbwValues.b = 100;
	concats[7].rgbwValues.w = 0;

	concats[8].rgbwValues.r = 0;
	concats[8].rgbwValues.g = 100;
	concats[8].rgbwValues.b = 0;
	concats[8].rgbwValues.w = 100;

	uint8_t concatsLength = 9;

	for(;;)
	{
		c = osMailAlloc(colorQueue, osWaitForever);
		if(c != NULL)
		{
			*c = concats[conc_index];
			conc_index = (conc_index+1)% concatsLength;

			if(osMailPut(colorQueue, c) != osOK)
			{
				osMailFree(colorQueue, c);
			}
		}

		osDelay(1000);
	}
}

void messyThread(void const * argument)
{
	for(;;)
	{
		BSP_LED_Toggle(LED1);
		osDelay(1);
	}
}

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow :
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 216000000
  *            HCLK(Hz)                       = 216000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 4
  *            APB2 Prescaler                 = 2
  *            HSE Frequency(Hz)              = 25000000
  *            PLL_M                          = 25
  *            PLL_N                          = 432
  *            PLL_P                          = 2
  *            PLL_Q                          = 9
  *            PLL_R                          = 7
  *            VDD(V)                         = 3.3
  *            Main regulator output voltage  = Scale1 mode
  *            Flash Latency(WS)              = 7
  * @param  None
  * @retval None
  */
static void SystemClock_Config(void)
{
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_OscInitTypeDef RCC_OscInitStruct;
	HAL_StatusTypeDef  ret = HAL_OK;

	/* Enable Power Control clock */
	__HAL_RCC_PWR_CLK_ENABLE();

	/* The voltage scaling allows optimizing the power consumption when the device is
       clocked below the maximum system frequency, to update the voltage scaling value
       regarding system frequency refer to product datasheet.  */
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	/* Enable HSE Oscillator and activate PLL with HSE as source */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 25;
	RCC_OscInitStruct.PLL.PLLN = 432;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 9;
	RCC_OscInitStruct.PLL.PLLR = 7;

	ret = HAL_RCC_OscConfig(&RCC_OscInitStruct);
	if(ret != HAL_OK)
	{
		while(1) { ; }
	}

	/* Activate the OverDrive to reach the 216 MHz Frequency */
	ret = HAL_PWREx_EnableOverDrive();
	if(ret != HAL_OK)
	{
		while(1) { ; }
	}

	/* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 clocks dividers */
	RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

	ret = HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7);
	if(ret != HAL_OK)
	{
		while(1) { ; }
	}
}
/**
  * @brief  Playback initialization
  * @param  None
  * @retval None
  */
static void Playback_Init(void)
{
	if(BSP_AUDIO_OUT_Init(OUTPUT_DEVICE_HEADPHONE, 20, AUDIO_FREQUENCY_11K) == AUDIO_ERROR) // TODO understand why 11kHz works here
	{
		Error_Handler(ERROR_AUDIO_INIT);
	}
	if(BSP_AUDIO_OUT_Play(PlayBuff, 2*PLAY_BUFF_SIZE) == AUDIO_ERROR)	// Size is in bytes
	{
		Error_Handler(ERROR_AUDIO_PLAY);
	}
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(ErrorCode e)
{
	/* LED1 On in error case */
	BSP_LED_On(LED1);
	while (1)
	{
	}
}

/**
  * @brief Tx Transfer completed callbacks.
  * @param  hsai : pointer to a SAI_HandleTypeDef structure that contains
  *                the configuration information for SAI module.
  * @retval None
  */
void BSP_AUDIO_OUT_TransferComplete_CallBack(void)
{
	uint16_t* val;
	val = osMailAlloc(sampleDMAQueue, 0);

	if(val != NULL)
	{
		*val = PLAY_BUFF_SIZE/2;

		if(osMailPut(sampleDMAQueue, val) != osOK)
		{
			osMailFree(sampleDMAQueue, val);
		}
	}
}

/**
  * @brief Tx Transfer Half completed callbacks
  * @param  hsai : pointer to a SAI_HandleTypeDef structure that contains
  *                the configuration information for SAI module.
  * @retval None
  */
void BSP_AUDIO_OUT_HalfTransfer_CallBack(void)
{
	uint16_t* val;
	val = osMailAlloc(sampleDMAQueue, 0);
	if(val != NULL)
	{
		*val = 0;
		if(osMailPut(sampleDMAQueue, val) != osOK)
		{
			osMailFree(sampleDMAQueue, val);
		}
	}
}

/**
* @brief  CPU L1-Cache enable.
* @param  None
* @retval None
*/
static void CPU_CACHE_Enable(void)
{
	/* Enable I-Cache */
	SCB_EnableICache();

	/* Enable D-Cache */
	SCB_EnableDCache();
}

void vApplicationStackOverflowHook( TaskHandle_t xTask, char *pcTaskName )
{
	Error_Handler(ERROR_STACK_OVERFLOW);
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
	/* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	/* Infinite loop */
	while (1)
	{
	}
}
#endif

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
